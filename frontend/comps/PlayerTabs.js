import Layout from '../comps/MyLayout.js'
import Link from 'next/link'
import fetch from 'isomorphic-unfetch'


const playerTabs = (props) => {
  let picLink = props.name.replace(" ", "-")
  return (
    <div>
      <Link as={`/f/${props.name}`} href={`/feed?id=${props.name}`}>
        <a>
          <p>{props.name}</p>
          <img src={`/static/images/${picLink}.png`}/>
        </a>
      </Link>
      <style jsx>{`
          div{
            position: static;
            margin: 90px;
          }
          a{
            color: #fc7307;
            margin: none;
            text-align: center;

          }
          a:hover{
            color: red;
          }
        `}
      </style>
    </div>
  )
}

export default playerTabs;
