import Link from 'next/link'

const linkStyle = {
  marginRight: 15
}

const Footer = () => (
  <div>
    <h3>Follow us on our Social Media </h3>
    <p className='Host'><Link href="https://www.zeit.co/now"><a>Now</a></Link></p>
    <p className='Twitter'><Link href="https://www.twitter.com/CebWatch"><a>Twitter</a></Link></p>
    <style jsx>{`
         div{
             font-size: 10px;
             text-align: center;
             margin: 75px;
         }
         h3{
             text-decoration: underline;
             font-weight: bold;
             color: #fc7307;
         }
         p{
             text-align:center;
             font-size: 10px;
             color: #fc7307;
             margin: 5px;
             font-weight: bold;
             text-decoration: underline;
         }
         .Host{
             margin: 15px;
         }
         .Twitter{
             margin: 15px;
         }
         a:hover{
             color: red;
         }
         a{
            color: #fc7307;
         }
           
        `}</style>
  </div>
)

export default Footer
