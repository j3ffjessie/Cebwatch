import Link from 'next/link'

const linkStyle = {
  marginRight: 15
}

const Header = () => (
  <div>
    <Link href="/">
      <a style={linkStyle}>Home</a>
    </Link>
    <Link href="/about">
      <a style={linkStyle}>About</a>
    </Link>
    <Link href="/contact">
      <a style={linkStyle}>Contact Us</a>
    </Link>

    <style jsx>{`
         div{
           font-size: 10px;
           width: 150px;
           height: 100px;
         }
         a:hover{
           color: red;
         }
         a{
          color: #fc7307;
         }
        `}</style>
  </div>
)

export default Header
