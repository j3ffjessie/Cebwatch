import Layout from '../comps/MyLayout.js'

export default () => (
  <Layout>
    <h1>About CebWatch</h1>
    <p>A social media and info aggregator for celebrities and athletes.</p>

    <style global jsx>{`
        body{
          background: #142739;
          color: #fc7307;
          background-image: linear-gradient(#091034, #559E54);
        }
        h1{
          color: #fc7307;
          text-align: center;
          margin: 30px;
          text-decoration: underline;
          text-shadow: 2px 2px black;
        }
        p{
          margin: 25px;
          text-align: center;
        }
        `}
        </style>
  </Layout>
)
