import {withRouter} from 'next/router'
import Layout from '../comps/MyLayout.js'

const Page = withRouter((props) => (
  <Layout>
    <h1>{props.router.query.id}</h1>
    <p>Social media feed</p>

    <style global jsx>{`
    h1{
      text-align: center;
      color: #fc7307;
      font-style: bold;
      text-decoration: underline;
      text-shadow: 2px 2px black;
    }
    p{
      text-align: center;
      text-decoration-line: overline underline;
      text-decoration-style: solid;
    }
    body{
      background: #142739;
      color: #fc7307;
      background-image: linear-gradient(#091034, #559E54);
        `}</style>
  </Layout>
))
/* we can also use the following text adjustments for the social media feed possibly
  text-align: justify;
  text-justify: inter-word; */
export default Page
