import Layout from '../comps/MyLayout.js'
import Link from 'next/link'
import PlayerTabs from '../comps/PlayerTabs.js'
import fetch from 'isomorphic-unfetch'
import footer from '../comps/Footer.js'
import props from '../comps/PlayerTabs.js'

const trendingPlayers = [
  {name: 'LeBron James',id: 1},
  {name: 'Kevin Durant', id: 2},
  {name: 'Anthony Davis', id: 3},
  {name: 'Stephen Curry', id: 4},
  {name: 'Giannis Antetokounmpo', id: 5},
  {name: 'Russell Westbrook', id: 6},
  {name: 'Chris Paul', id: 7},
  {name: 'Kawhi Leonard', id: 8},
  {name: 'James Harden', id: 9},
  {name: 'Jimmy Butler', id: 10}
]

const Index = () => (
  <Layout>
    <h1>Trending Players</h1>
     {trendingPlayers.map((players) => {
      return <PlayerTabs
        name={players.name}
        key={players.id}/>})}

        <style global jsx>{`
            h1 {
              color: #fc7307;
              text-align: center;
              margin: 30px;
              text-decoration: underline;
              text-shadow: 2px 2px black;
            }
            .row::after {
              content: "";
              clear: both;
              display: table;
            }
            .column{
              text-align: center;
              width: 100%;
              padding: 5px;
            }
            div{
              text-align: center;
            }
            img{
              width: 25%;
              height: 25%;
              display: in-line;
              border-radius: 25%;
              margin: 0 auto;
              border: 2px solid #ffffff;
              text-align: center;
            }
            html{
              scroll-behavior: smooth;
            }

            players:hover {
              opacity: 0.6;
              pointer: crosshairs;
            }
            body{
              background: #142739;
              color: #ff3300;
              background-image: linear-gradient(#091034, #559E54);
            }
        `}</style>
    </Layout>
)


export default Index
