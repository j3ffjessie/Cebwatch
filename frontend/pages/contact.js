import Layout from '../comps/MyLayout.js'
import Link from 'next/link'


export default () => (
  <Layout>
    <h1>Contact Our Team</h1>
    <div className='contact'>
    <p><Link href="https://github.com/"><a>Cameron</a></Link></p>
    <p><Link href="https://github.com/"><a>Scott</a></Link></p>
    <p><Link href="https://github.com/"><a>Joe</a></Link></p>
    <p><Link href="https://github.com/J3ffJessie"><a>Jeff Jessie</a></Link></p>
    <p><Link href="https://github.com/"><a>Somraj</a></Link></p>
    <h2>Email Us</h2>
    <p1> Email us your suggestions.  Want another sport added to the site? Want different stats to be shown for your favorite athletes? 
        Let us know by emailing us at <a href="mailto:admin@cebwatch.com?&subject=Suggestion for the Team">admin@cebwatch.com</a>.  Thank you for visiting the site and taking time to provide us with feedback.  It is greatly appreciated.</p1>
</div>
    <style global jsx>{`
          body{
            background: #142739;
            background-image: linear-gradient(#091034, #559E54);
          }
          h1{
              text-align: center;
              color: #fc7307;
              margin: 30px;
              text-decoration: underline;
              font-weight: bold;
              text-shadow: 2px 2px black;
          }
          p{
            text-align: center;
            font-size: 16px;
            animation: move 10s steps(10) infinite alternate;
          }
          .contact{
              text-align: center;
              font-size: 12px;
              color: #fc7307;
            }
            h2{
                text-align: center;
                font-size: 20px;
                color: #fc7307;
                margin: 30px;
                text-decoration: underline;
                font-weight: bold;
                font-style: italic;
            }
            p1{
                text-align: center;
                font-size: 12px;
                color: white;
            }
            a {
                color: #fc7307;
            }
            a:hover{
                color: red;
            }
        `}</style>

  </Layout>
)
