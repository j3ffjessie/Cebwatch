module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./comps/Footer.js":
/*!*************************!*\
  !*** ./comps/Footer.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);



var linkStyle = {
  marginRight: 15
};

var Footer = function Footer() {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-2608948896"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "jsx-2608948896"
  }, "Follow us on our Social Media "), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "jsx-2608948896" + " " + 'Host'
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "https://www.zeit.co/now"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    className: "jsx-2608948896"
  }, "Now"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "jsx-2608948896" + " " + 'Twitter'
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "https://www.twitter.com/CebWatch"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    className: "jsx-2608948896"
  }, "Twitter"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    styleId: "2608948896",
    css: "div.jsx-2608948896{font-size:10px;text-align:center;margin:75px;}h3.jsx-2608948896{-webkit-text-decoration:underline;text-decoration:underline;font-weight:bold;color:#fc7307;}p.jsx-2608948896{text-align:center;font-size:10px;color:#fc7307;margin:5px;font-weight:bold;-webkit-text-decoration:underline;text-decoration:underline;}.Host.jsx-2608948896{margin:15px;}.Twitter.jsx-2608948896{margin:15px;}a.jsx-2608948896:hover{color:red;}a.jsx-2608948896{color:#fc7307;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcSmVmZlxcRGVza3RvcFxcQ2Vid2F0Y2hcXGNlYndhdGNoXFxmcm9udGVuZFxcY29tcHNcXEZvb3Rlci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFXZ0IsQUFHNkIsQUFLVyxBQUtULEFBUUwsQUFHQSxBQUdGLEFBR0csVUFGakIsRUFOQSxBQUdBLEVBTUEsQ0EzQnNCLEdBVUgsZUFUSCxBQVVFLFlBVGxCLEVBVWUsV0FDTSxFQVJBLGVBU1MsRUFSWixjQUNsQiw0Q0FRQSIsImZpbGUiOiJDOlxcVXNlcnNcXEplZmZcXERlc2t0b3BcXENlYndhdGNoXFxjZWJ3YXRjaFxcZnJvbnRlbmRcXGNvbXBzXFxGb290ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnXHJcblxyXG5jb25zdCBsaW5rU3R5bGUgPSB7XHJcbiAgbWFyZ2luUmlnaHQ6IDE1XHJcbn1cclxuXHJcbmNvbnN0IEZvb3RlciA9ICgpID0+IChcclxuICA8ZGl2PlxyXG4gICAgPGgzPkZvbGxvdyB1cyBvbiBvdXIgU29jaWFsIE1lZGlhIDwvaDM+XHJcbiAgICA8cCBjbGFzc05hbWU9J0hvc3QnPjxMaW5rIGhyZWY9XCJodHRwczovL3d3dy56ZWl0LmNvL25vd1wiPjxhPk5vdzwvYT48L0xpbms+PC9wPlxyXG4gICAgPHAgY2xhc3NOYW1lPSdUd2l0dGVyJz48TGluayBocmVmPVwiaHR0cHM6Ly93d3cudHdpdHRlci5jb20vQ2ViV2F0Y2hcIj48YT5Ud2l0dGVyPC9hPjwvTGluaz48L3A+XHJcbiAgICA8c3R5bGUganN4PntgXHJcbiAgICAgICAgIGRpdntcclxuICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgIG1hcmdpbjogNzVweDtcclxuICAgICAgICAgfVxyXG4gICAgICAgICBoM3tcclxuICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICBjb2xvcjogI2ZjNzMwNztcclxuICAgICAgICAgfVxyXG4gICAgICAgICBwe1xyXG4gICAgICAgICAgICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgICBjb2xvcjogI2ZjNzMwNztcclxuICAgICAgICAgICAgIG1hcmdpbjogNXB4O1xyXG4gICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICAgICAgICAgfVxyXG4gICAgICAgICAuSG9zdHtcclxuICAgICAgICAgICAgIG1hcmdpbjogMTVweDtcclxuICAgICAgICAgfVxyXG4gICAgICAgICAuVHdpdHRlcntcclxuICAgICAgICAgICAgIG1hcmdpbjogMTVweDtcclxuICAgICAgICAgfVxyXG4gICAgICAgICBhOmhvdmVye1xyXG4gICAgICAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICAgfVxyXG4gICAgICAgICBhe1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZjNzMwNztcclxuICAgICAgICAgfVxyXG4gICAgICAgICAgIFxyXG4gICAgICAgIGB9PC9zdHlsZT5cclxuICA8L2Rpdj5cclxuKVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRm9vdGVyXHJcbiJdfQ== */\n/*@ sourceURL=C:\\Users\\Jeff\\Desktop\\Cebwatch\\cebwatch\\frontend\\comps\\Footer.js */"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./comps/Header.js":
/*!*************************!*\
  !*** ./comps/Header.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);



var linkStyle = {
  marginRight: 15
};

var Header = function Header() {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-7284294"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    style: linkStyle,
    className: "jsx-7284294"
  }, "Home")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/about"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    style: linkStyle,
    className: "jsx-7284294"
  }, "About")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/contact"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    style: linkStyle,
    className: "jsx-7284294"
  }, "Contact Us")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    styleId: "7284294",
    css: "div.jsx-7284294{font-size:10px;width:150px;height:100px;}a.jsx-7284294:hover{color:red;}a.jsx-7284294{color:#fc7307;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcSmVmZlxcRGVza3RvcFxcQ2Vid2F0Y2hcXGNlYndhdGNoXFxmcm9udGVuZFxcY29tcHNcXEhlYWRlci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFrQmdCLEFBRzJCLEFBS0wsQUFHRyxVQUZmLElBR0EsQ0FSYyxZQUNDLGFBQ2YiLCJmaWxlIjoiQzpcXFVzZXJzXFxKZWZmXFxEZXNrdG9wXFxDZWJ3YXRjaFxcY2Vid2F0Y2hcXGZyb250ZW5kXFxjb21wc1xcSGVhZGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IExpbmsgZnJvbSAnbmV4dC9saW5rJ1xyXG5cclxuY29uc3QgbGlua1N0eWxlID0ge1xyXG4gIG1hcmdpblJpZ2h0OiAxNVxyXG59XHJcblxyXG5jb25zdCBIZWFkZXIgPSAoKSA9PiAoXHJcbiAgPGRpdj5cclxuICAgIDxMaW5rIGhyZWY9XCIvXCI+XHJcbiAgICAgIDxhIHN0eWxlPXtsaW5rU3R5bGV9PkhvbWU8L2E+XHJcbiAgICA8L0xpbms+XHJcbiAgICA8TGluayBocmVmPVwiL2Fib3V0XCI+XHJcbiAgICAgIDxhIHN0eWxlPXtsaW5rU3R5bGV9PkFib3V0PC9hPlxyXG4gICAgPC9MaW5rPlxyXG4gICAgPExpbmsgaHJlZj1cIi9jb250YWN0XCI+XHJcbiAgICAgIDxhIHN0eWxlPXtsaW5rU3R5bGV9PkNvbnRhY3QgVXM8L2E+XHJcbiAgICA8L0xpbms+XHJcblxyXG4gICAgPHN0eWxlIGpzeD57YFxyXG4gICAgICAgICBkaXZ7XHJcbiAgICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgICAgIHdpZHRoOiAxNTBweDtcclxuICAgICAgICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgICAgICB9XHJcbiAgICAgICAgIGE6aG92ZXJ7XHJcbiAgICAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICAgfVxyXG4gICAgICAgICBhe1xyXG4gICAgICAgICAgY29sb3I6ICNmYzczMDc7XHJcbiAgICAgICAgIH1cclxuICAgICAgICBgfTwvc3R5bGU+XHJcbiAgPC9kaXY+XHJcbilcclxuXHJcbmV4cG9ydCBkZWZhdWx0IEhlYWRlclxyXG4iXX0= */\n/*@ sourceURL=C:\\Users\\Jeff\\Desktop\\Cebwatch\\cebwatch\\frontend\\comps\\Header.js */"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./comps/MyLayout.js":
/*!***************************!*\
  !*** ./comps/MyLayout.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Header */ "./comps/Header.js");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Footer */ "./comps/Footer.js");



var layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #DDD'
};

var Layout = function Layout(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: layoutStyle
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_1__["default"], null), props.children, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_2__["default"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (Layout);

/***/ }),

/***/ "./comps/PlayerTabs.js":
/*!*****************************!*\
  !*** ./comps/PlayerTabs.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _comps_MyLayout_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../comps/MyLayout.js */ "./comps/MyLayout.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__);






var playerTabs = function playerTabs(props) {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-1778172009"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_3___default.a, {
    as: "/f/".concat(props.name),
    href: "/feed?id=".concat(props.name)
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    className: "jsx-1778172009"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "jsx-1778172009"
  }, props.name))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    styleId: "1778172009",
    css: "div.jsx-1778172009{position:static;margin:none;}a.jsx-1778172009{color:#fc7307;margin:none;text-align:center;}a.jsx-1778172009:hover{color:red;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcSmVmZlxcRGVza3RvcFxcQ2Vid2F0Y2hcXGNlYndhdGNoXFxmcm9udGVuZFxcY29tcHNcXFBsYXllclRhYnMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBVWtCLEFBRzZCLEFBSUYsQUFLSixVQUNaLElBTGMsRUFKQSxVQUtNLEVBSnBCLGdCQUtBIiwiZmlsZSI6IkM6XFxVc2Vyc1xcSmVmZlxcRGVza3RvcFxcQ2Vid2F0Y2hcXGNlYndhdGNoXFxmcm9udGVuZFxcY29tcHNcXFBsYXllclRhYnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTGF5b3V0IGZyb20gJy4uL2NvbXBzL015TGF5b3V0LmpzJ1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnXHJcbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLXVuZmV0Y2gnXHJcblxyXG5jb25zdCBwbGF5ZXJUYWJzID0gKHByb3BzKSA9PiB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxkaXY+XHJcbiAgICAgIDxMaW5rIGFzPXtgL2YvJHtwcm9wcy5uYW1lfWB9IGhyZWY9e2AvZmVlZD9pZD0ke3Byb3BzLm5hbWV9YH0+XHJcbiAgICAgICAgPGE+PHA+e3Byb3BzLm5hbWV9PC9wPjwvYT5cclxuICAgICAgPC9MaW5rPlxyXG4gICAgICA8c3R5bGUganN4PntgXHJcbiAgICAgICAgICBkaXZ7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBzdGF0aWM7XHJcbiAgICAgICAgICAgIG1hcmdpbjogbm9uZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGF7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmM3MzA3O1xyXG4gICAgICAgICAgICBtYXJnaW46IG5vbmU7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGE6aG92ZXJ7XHJcbiAgICAgICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgYH1cclxuICAgICAgPC9zdHlsZT5cclxuICAgIDwvZGl2PlxyXG4gIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgcGxheWVyVGFicztcclxuIl19 */\n/*@ sourceURL=C:\\Users\\Jeff\\Desktop\\Cebwatch\\cebwatch\\frontend\\comps\\PlayerTabs.js */"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (playerTabs);

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _comps_MyLayout_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../comps/MyLayout.js */ "./comps/MyLayout.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../comps/PlayerTabs.js */ "./comps/PlayerTabs.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _comps_Footer_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../comps/Footer.js */ "./comps/Footer.js");








var trendingPlayers = [{
  name: 'LeBron James',
  id: 1
}, {
  name: 'Kevin Durant',
  id: 2
}, {
  name: 'Anthony Davis',
  id: 3
}, {
  name: 'Stephen Curry',
  id: 4
}, {
  name: 'Giannis Antetokounmpo',
  id: 5
}, {
  name: 'Russell Westbrook',
  id: 6
}, {
  name: 'Chris Paul',
  id: 7
}, {
  name: 'Kawhi Leonard',
  id: 8
}, {
  name: 'James Harden',
  id: 9
}, {
  name: 'Jimmy Butler',
  id: 10
}];

var Index = function Index() {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_comps_MyLayout_js__WEBPACK_IMPORTED_MODULE_2__["default"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "jsx-1009519110"
  }, "Trending Players"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-1009519110" + " " + "row"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-1009519110" + " " + "column"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/lebron-james.png",
    alt: "Lebron James-Los Angeles Lakers",
    className: "jsx-1009519110"
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/kevin-durant.png",
    alt: "Kevin Durant-Golden State Warriors",
    className: "jsx-1009519110"
  }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-1009519110" + " " + "column"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/anthony-davis.png",
    alt: "Anthony Davis- New Orleans Pelicans",
    className: "jsx-1009519110"
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/steph-curry.png",
    alt: "Stephen Curry- Golden State Warriors",
    className: "jsx-1009519110"
  }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-1009519110" + " " + "column"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/giannis-antetokounmpo.png",
    alt: "Giannis Antetokounmpo-Milwaukee Bucks",
    className: "jsx-1009519110"
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/russell-westbrook.png",
    alt: "Russell Westbrook- Oklahoma City Thunder",
    className: "jsx-1009519110"
  }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-1009519110" + " " + "column"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/chris-paul.png",
    alt: "Chris Paul- Houston Rockets",
    className: "jsx-1009519110"
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/kawhi-leonard.png",
    alt: "Kawhi Leonard-Toronto Raptors",
    className: "jsx-1009519110"
  }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-1009519110" + " " + "column"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/james-harden.png",
    alt: "James Harden- Houston Rockets",
    className: "jsx-1009519110"
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/f/".concat(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"].name),
    className: "jsx-1009519110"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/static/images/jimmy-butler.png",
    alt: "Jimmy Butler-Philadelphia 76ers",
    className: "jsx-1009519110"
  })))), trendingPlayers.map(function (players) {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_comps_PlayerTabs_js__WEBPACK_IMPORTED_MODULE_4__["default"], {
      name: players.name,
      key: players.id
    });
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    styleId: "1009519110",
    css: "h1{color:#fc7307;text-align:center;margin:30px;-webkit-text-decoration:underline;text-decoration:underline;text-shadow:2px 2px black;}.row::after{content:\"\";clear:both;display:table;}.column{text-align:center;width:100%;padding:5px;}div{text-align:center;}img{width:25%;height:25%;display:in-line;border-radius:25%;margin:0 auto;border:2px solid #ffffff;text-align:center;}html{-webkit-scroll-behavior:smooth;-moz-scroll-behavior:smooth;-ms-scroll-behavior:smooth;scroll-behavior:smooth;}players:hover{opacity:0.6;pointer:crosshairs;}body{background:#142739;color:#ff3300;background-image:linear-gradient(#091034,#559E54);}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcSmVmZlxcRGVza3RvcFxcQ2Vid2F0Y2hcXGNlYndhdGNoXFxmcm9udGVuZFxccGFnZXNcXGluZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQW1EMkIsQUFHNkIsQUFPSCxBQUtPLEFBS0EsQUFHUixBQVNhLEFBSVgsQUFJTyxVQWhCUixDQWJBLENBMEJRLEVBakNELElBWVAsQUFLYixDQW9CZ0IsRUFoQkUsQ0FiRixPQUtGLEVBcUJkLENBakNjLENBcUN1QyxHQTdCckQsQ0Fhb0IsSUFScEIsR0FaNEIsV0FxQlosY0FDVyxjQWUzQixXQWRvQixVQXRCUSxLQTBCNUIsR0FIQSxrQkF0QkEiLCJmaWxlIjoiQzpcXFVzZXJzXFxKZWZmXFxEZXNrdG9wXFxDZWJ3YXRjaFxcY2Vid2F0Y2hcXGZyb250ZW5kXFxwYWdlc1xcaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTGF5b3V0IGZyb20gJy4uL2NvbXBzL015TGF5b3V0LmpzJ1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnXHJcbmltcG9ydCBQbGF5ZXJUYWJzIGZyb20gJy4uL2NvbXBzL1BsYXllclRhYnMuanMnXHJcbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLXVuZmV0Y2gnXHJcbmltcG9ydCBmb290ZXIgZnJvbSAnLi4vY29tcHMvRm9vdGVyLmpzJ1xyXG5pbXBvcnQgcHJvcHMgZnJvbSAnLi4vY29tcHMvUGxheWVyVGFicy5qcydcclxuXHJcbmNvbnN0IHRyZW5kaW5nUGxheWVycyA9IFtcclxuICB7bmFtZTogJ0xlQnJvbiBKYW1lcycsaWQ6IDF9LFxyXG4gIHtuYW1lOiAnS2V2aW4gRHVyYW50JywgaWQ6IDJ9LFxyXG4gIHtuYW1lOiAnQW50aG9ueSBEYXZpcycsIGlkOiAzfSxcclxuICB7bmFtZTogJ1N0ZXBoZW4gQ3VycnknLCBpZDogNH0sXHJcbiAge25hbWU6ICdHaWFubmlzIEFudGV0b2tvdW5tcG8nLCBpZDogNX0sXHJcbiAge25hbWU6ICdSdXNzZWxsIFdlc3Ricm9vaycsIGlkOiA2fSxcclxuICB7bmFtZTogJ0NocmlzIFBhdWwnLCBpZDogN30sXHJcbiAge25hbWU6ICdLYXdoaSBMZW9uYXJkJywgaWQ6IDh9LFxyXG4gIHtuYW1lOiAnSmFtZXMgSGFyZGVuJywgaWQ6IDl9LFxyXG4gIHtuYW1lOiAnSmltbXkgQnV0bGVyJywgaWQ6IDEwfVxyXG5dXHJcblxyXG5jb25zdCBJbmRleCA9ICgpID0+IChcclxuICA8TGF5b3V0PlxyXG4gICAgPGgxPlRyZW5kaW5nIFBsYXllcnM8L2gxPlxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sdW1uXCI+XHJcbiAgICAgICAgICA8YSBocmVmPXtgL2YvJHtwcm9wcy5uYW1lfWB9PjxpbWcgc3JjPVwiL3N0YXRpYy9pbWFnZXMvbGVicm9uLWphbWVzLnBuZ1wiIGFsdD1cIkxlYnJvbiBKYW1lcy1Mb3MgQW5nZWxlcyBMYWtlcnNcIiAvPjwvYT5cclxuICAgICAgICAgIDxhIGhyZWY9e2AvZi8ke3Byb3BzLm5hbWV9YH0+PGltZyBzcmM9XCIvc3RhdGljL2ltYWdlcy9rZXZpbi1kdXJhbnQucG5nXCIgYWx0PVwiS2V2aW4gRHVyYW50LUdvbGRlbiBTdGF0ZSBXYXJyaW9yc1wiIC8+PC9hPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbHVtblwiPlxyXG4gICAgICAgICAgPGEgaHJlZj17YC9mLyR7cHJvcHMubmFtZX1gfT48aW1nIHNyYz1cIi9zdGF0aWMvaW1hZ2VzL2FudGhvbnktZGF2aXMucG5nXCIgYWx0PVwiQW50aG9ueSBEYXZpcy0gTmV3IE9ybGVhbnMgUGVsaWNhbnNcIiAvPjwvYT5cclxuICAgICAgICAgIDxhIGhyZWY9e2AvZi8ke3Byb3BzLm5hbWV9YH0+PGltZyBzcmM9XCIvc3RhdGljL2ltYWdlcy9zdGVwaC1jdXJyeS5wbmdcIiBhbHQ9XCJTdGVwaGVuIEN1cnJ5LSBHb2xkZW4gU3RhdGUgV2FycmlvcnNcIiAvPjwvYT5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2x1bW5cIj5cclxuICAgICAgICAgIDxhIGhyZWY9e2AvZi8ke3Byb3BzLm5hbWV9YH0+PGltZyBzcmM9XCIvc3RhdGljL2ltYWdlcy9naWFubmlzLWFudGV0b2tvdW5tcG8ucG5nXCIgYWx0PVwiR2lhbm5pcyBBbnRldG9rb3VubXBvLU1pbHdhdWtlZSBCdWNrc1wiIC8+PC9hPlxyXG4gICAgICAgICAgPGEgaHJlZj17YC9mLyR7cHJvcHMubmFtZX1gfT48aW1nIHNyYz1cIi9zdGF0aWMvaW1hZ2VzL3J1c3NlbGwtd2VzdGJyb29rLnBuZ1wiIGFsdD1cIlJ1c3NlbGwgV2VzdGJyb29rLSBPa2xhaG9tYSBDaXR5IFRodW5kZXJcIiAvPjwvYT5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2x1bW5cIj5cclxuICAgICAgICAgIDxhIGhyZWY9e2AvZi8ke3Byb3BzLm5hbWV9YH0+PGltZyBzcmM9XCIvc3RhdGljL2ltYWdlcy9jaHJpcy1wYXVsLnBuZ1wiIGFsdD1cIkNocmlzIFBhdWwtIEhvdXN0b24gUm9ja2V0c1wiIC8+PC9hPlxyXG4gICAgICAgICAgPGEgaHJlZj17YC9mLyR7cHJvcHMubmFtZX1gfT48aW1nIHNyYz1cIi9zdGF0aWMvaW1hZ2VzL2thd2hpLWxlb25hcmQucG5nXCIgYWx0PVwiS2F3aGkgTGVvbmFyZC1Ub3JvbnRvIFJhcHRvcnNcIiAvPjwvYT5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2x1bW5cIj5cclxuICAgICAgICAgIDxhIGhyZWY9e2AvZi8ke3Byb3BzLm5hbWV9YH0+PGltZyBzcmM9XCIvc3RhdGljL2ltYWdlcy9qYW1lcy1oYXJkZW4ucG5nXCIgYWx0PVwiSmFtZXMgSGFyZGVuLSBIb3VzdG9uIFJvY2tldHNcIiAvPjwvYT5cclxuICAgICAgICAgIDxhIGhyZWY9e2AvZi8ke3Byb3BzLm5hbWV9YH0+PGltZyBzcmM9XCIvc3RhdGljL2ltYWdlcy9qaW1teS1idXRsZXIucG5nXCIgYWx0PVwiSmltbXkgQnV0bGVyLVBoaWxhZGVscGhpYSA3NmVyc1wiIC8+PC9hPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgIHt0cmVuZGluZ1BsYXllcnMubWFwKChwbGF5ZXJzKSA9PiB7XHJcbiAgICAgIHJldHVybiA8UGxheWVyVGFic1xyXG4gICAgICAgIG5hbWU9e3BsYXllcnMubmFtZX1cclxuICAgICAgICBrZXk9e3BsYXllcnMuaWR9Lz59KX1cclxuXHJcbiAgICAgICAgPHN0eWxlIGdsb2JhbCBqc3g+e2BcclxuICAgICAgICAgICAgaDEge1xyXG4gICAgICAgICAgICAgIGNvbG9yOiAjZmM3MzA3O1xyXG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICBtYXJnaW46IDMwcHg7XHJcbiAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbiAgICAgICAgICAgICAgdGV4dC1zaGFkb3c6IDJweCAycHggYmxhY2s7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLnJvdzo6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgY2xlYXI6IGJvdGg7XHJcbiAgICAgICAgICAgICAgZGlzcGxheTogdGFibGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNvbHVtbntcclxuICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGRpdntcclxuICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICAgIHdpZHRoOiAyNSU7XHJcbiAgICAgICAgICAgICAgaGVpZ2h0OiAyNSU7XHJcbiAgICAgICAgICAgICAgZGlzcGxheTogaW4tbGluZTtcclxuICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyNSU7XHJcbiAgICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgICAgICAgYm9yZGVyOiAycHggc29saWQgI2ZmZmZmZjtcclxuICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaHRtbHtcclxuICAgICAgICAgICAgICBzY3JvbGwtYmVoYXZpb3I6IHNtb290aDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcGxheWVyczpob3ZlciB7XHJcbiAgICAgICAgICAgICAgb3BhY2l0eTogMC42O1xyXG4gICAgICAgICAgICAgIHBvaW50ZXI6IGNyb3NzaGFpcnM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYm9keXtcclxuICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMTQyNzM5O1xyXG4gICAgICAgICAgICAgIGNvbG9yOiAjZmYzMzAwO1xyXG4gICAgICAgICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgjMDkxMDM0LCAjNTU5RTU0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIGB9PC9zdHlsZT5cclxuICAgIDwvTGF5b3V0PlxyXG4pXHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgSW5kZXhcclxuIl19 */\n/*@ sourceURL=C:\\Users\\Jeff\\Desktop\\Cebwatch\\cebwatch\\frontend\\pages\\index.js */"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./pages/index.js */"./pages/index.js");


/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "next/link":
/*!****************************!*\
  !*** external "next/link" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map